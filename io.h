#include <netdb.h>

#define CONN_VALID 0
#define CONN_ERROR -1
#define CONN_RETRY -2
#define CONN_ABORT -3

struct cnx {
#ifdef USE_TLS
	struct tls *tls;
#endif
	int sock;
};

extern int tls;

extern int (*iosetup)(void);
extern int (*ioclose)(struct cnx *);
extern int (*ioconnect)(struct cnx *, struct addrinfo *, const char *);
extern void (*ioconnerr)(struct cnx *, const char *, const char *, int);
extern char *(*ioparseurl)(char *);
extern ssize_t (*ioread)(struct cnx *, void *, size_t);
extern ssize_t (*iowrite)(struct cnx *, void *, size_t);
